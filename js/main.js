$(function () {
    $('#toggle-menu').click(function() {
        $(this).toggleClass('active');
        $('#sidebar').toggleClass('active');
    });

    $ ('.giv-oskar').click(function (e) {
        $(this).parents('.modal').modal('hide');
    });

    $('.column').click(function () {
        $(this).addClass('active');
        $('.column-input').focus();
    });

    $('.column-input').blur(function () {
        $(this).parents('.column').removeClass('active');
    });
});

